<?php

namespace Atreo\Calendar;

use Nette\Object;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 * @property-read \DateTime $date
 * @property-read string $dayName
 * @property-read DayInterval[] $dayIntervals
 */
class Day extends Object
{

	/**
	 * @var array
	 */
	private static $dateNames = array(
		1 => 'Pondělí',
		2 => 'Úterý',
		3 => 'Středa',
		4 => 'Čtvrtek',
		5 => 'Pátek',
		6 => 'Sobota',
		7 => 'Neděle'
	);

	/**
	 * @var \DateTime
	 */
	private $date;

	/**
	 * @var string
	 */
	private $dayName;

	/**
	 * @var array
	 */
	private $dayIntervals = array();



	/**
	 * @param \DateTime $date
	 */
	function __construct(\DateTime $date)
	{
		$this->date = $date;
		$this->date->setTime(7, 0, 0);
		$this->dayName = self::$dateNames[$this->date->format('N')];
	}



	/**
	 * @param \DateTime $from
	 * @param \DateTime $to
	 */
	public function addDayInterval(\DateTime $from, \DateTime $to)
	{
		$this->dayIntervals[] = new DayInterval($from, $to);
	}



	/**
	 * @param \DateTime $when
	 * @return \Atreo\Calendar\DayInterval
	 * @throws \Atreo\Calendar\DayIntervalNotFoundException
	 */
	public function getDayInterval(\DateTime $when)
	{
		foreach ($this->dayIntervals as $dayInterval) {
			if ($when >= $dayInterval->from && $when < $dayInterval->to) {
				return $dayInterval;
			}
		}

		throw new DayIntervalNotFoundException();
	}



	/**
	 * @return \DateTime
	 */
	public function getDate()
	{
		return $this->date;
	}



	/**
	 * @return string
	 */
	public function getDayName()
	{
		return $this->dayName;
	}



	/**
	 * @return array
	 */
	public function getDayIntervals()
	{
		return $this->dayIntervals;
	}



	/**
	 * @return array
	 */
	public static function getDateNames()
	{
		return self::$dateNames;
	}

}



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class DayIntervalNotFoundException extends CalendarException {}
