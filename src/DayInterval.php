<?php

namespace Atreo\Calendar;

use Nette\Object;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 * @property-read \DateTime $from
 * @property-read \DateTime $to
 * @property-read array $items
 */
class DayInterval extends Object
{

	/**
	 * @var \DateTime
	 */
	protected $from;

	/**
	 * @var \DateTime
	 */
	protected $to;

	/**
	 * @var array
	 */
	protected $items;



	/**
	 * @param \DateTime $from
	 * @param \DateTime $to
	 */
	public function __construct(\DateTime $from, \DateTime $to)
	{
		$this->from = $from;
		$this->to = $to;
	}



	/**
	 * @param mixed $item
	 */
	public function addItem($item)
	{
		$this->items[] = $item;
	}



	/**
	 * @return array
	 */
	public function getItems()
	{
		return $this->items;
	}



	/**
	 * @return \DateTime
	 */
	public function getFrom()
	{
		return $this->from;
	}



	/**
	 * @return \DateTime
	 */
	public function getTo()
	{
		return $this->to;
	}

}
