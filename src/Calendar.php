<?php

namespace Atreo\Calendar;

use Atreo\UI\Control;
use Nette\ComponentModel\IContainer;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 *
 * @property-read integer $numOfDaysOnPage
 * @property-read \DateTime $startDate
 * @property-read \DateTime $endDate
 * @property-read Day[] $days
 * @property-read DayInterval[] $dayIntervals
 */
class Calendar extends Control
{

	/**
	 * @persistent
	 * @var integer
	 */
	public $offset = 0;

	/**
	 * @var integer
	 */
	private $numOfDaysOnPage = 7;

	/**
	 * @var string
	 */
	private $dayIntervalTemplatePath;

	/**
	 * @var \DateTime
	 */
	private $startDate;

	/**
	 * @var \DateTime
	 */
	private $endDate;

	/**
	 * @var DayInterval[]
	 */
	private $dayIntervals = [];

	/**
	 * @var \Atreo\Calendar\Day[]
	 */
	private $days = array();



	/**
	 * @param \Nette\ComponentModel\IContainer $parent
	 * @param string|NULL $name
	 * @throws \Atreo\Calendar\CalendarException
	 */
	public function __construct(IContainer $parent = NULL, $name = NULL)
	{
		if ($parent === NULL) {
			throw new CalendarException("Please provide a presenter.");
		}

		if ($name == NULl) {
			$name = 'atreo-calendar';
		}

		parent::__construct($parent, $name);
		$this->prepareDays();
	}



	public function render()
	{
		$this->template->days = $this->days;
		$this->template->dayIntervals = $this->dayIntervals;
		$this->template->offset = $this->getParameter('offset');
		$this->template->numOfDaysOnPage = $this->numOfDaysOnPage;

		$this->template->startDate = $this->startDate;
		$this->template->endDate = $this->endDate;

		$this->template->previousStartDate = clone $this->startDate;
		$this->template->previousEndDate = clone $this->startDate;
		$this->template->previousStartDate->modify("- {$this->numOfDaysOnPage} days");
		$this->template->previousEndDate->modify('- 1 day');

		$this->template->nextStartDate = clone $this->endDate;
		$this->template->nextEndDate = clone $this->endDate;
		$this->template->nextStartDate->modify('+ 1 day');
		$this->template->nextEndDate->modify("+ {$this->numOfDaysOnPage} days");

		$this->template->setFile(__DIR__ . '/calendar.latte');
		$this->template->render();
	}



	public function renderDayInterval(DayInterval $dayInterval)
	{
		$this->template->dayInterval = $dayInterval;
		$this->template->setFile($this->dayIntervalTemplatePath);
		$this->template->render();
	}



	/********************* functions *********************/


	/**
	 * @param \DateTime $when
	 * @param mixed $item
	 * @throws \Atreo\Calendar\DayIntervalNotFoundException
	 */
	public function addItem(\DateTime $when, $item)
	{
		$this->days[$when->format('Ymd')]->getDayInterval($when)->addItem($item);
	}



	protected function prepareDays()
	{
		$monday = new \DateTime('monday this week');

		for ($i = 0; $i <= ($this->numOfDaysOnPage - 1); $i++) {
			$offset = $i + $this->getParameter('offset') * $this->numOfDaysOnPage;

			$date = clone $monday;

			if ($offset > 0) {
				$date->modify("+ {$offset} days");
			} elseif ($offset < 0) {
				$offset = abs($offset);
				$date->modify("- {$offset} days");
			}

			if ($i == 0) {
				$this->startDate = clone $date;
				$this->startDate->setTime(0, 0, 0);
			}
			if ($i == 6) {
				$this->endDate = clone $date;
				$this->endDate->setTime(23, 59, 59);
			}

			$this->days[$date->format('Ymd')] = new Day($date);;
		}
	}



	/**
	 * @param \DateTime $dayIntervalFrom
	 * @param \DateTime $dayIntervalTo
	 * @param integer $dayIntervalStep
	 * @throws \Atreo\Calendar\InvalidDayIntervalDatesException
	 */
	public function setDayIntervalsRange(\DateTime $dayIntervalFrom, \DateTime $dayIntervalTo, $dayIntervalStep)
	{
		if ($dayIntervalFrom > $dayIntervalTo || $dayIntervalFrom->format('Ymd') !== $dayIntervalTo->format('Ymd')) {
			throw new InvalidDayIntervalDatesException();
		}

		$intervalDate = clone $dayIntervalFrom;
		while ($intervalDate <= $dayIntervalTo) {

			$intervalDateFrom = clone $intervalDate;
			$intervalDateTo = clone $intervalDate;
			$intervalDateTo->modify("+ {$dayIntervalStep} minutes");

			$this->dayIntervals[] = new DayInterval($intervalDateFrom, $intervalDateTo);
			$intervalDate->modify("+ {$dayIntervalStep} minutes");
		}

		$this->prepareDayIntervals();
	}



	protected function prepareDayIntervals()
	{
		foreach ($this->days as $day) {
			foreach ($this->dayIntervals as $dayInterval) {

				$from = clone $dayInterval->from;
				$from->setDate($day->date->format('Y'), $day->date->format('m'), $day->date->format('d'));
				$to = clone $dayInterval->to;
				$to->setDate($day->date->format('Y'), $day->date->format('m'), $day->date->format('d'));

				$day->addDayInterval($from, $to);
			}
		}
	}



	/**
	 * @return integer
	 */
	public function getNumOfDaysOnPage()
	{
		return $this->numOfDaysOnPage;
	}



	/**
	 * @param int $numOfDaysOnPage
	 * @return $this
	 */
	public function setNumOfDaysOnPage($numOfDaysOnPage)
	{
		$this->numOfDaysOnPage = $numOfDaysOnPage;
		return $this;
	}



	/**
	 * @return string
	 */
	public function getItemFilePath()
	{
		return $this->dayIntervalTemplatePath;
	}



	/**
	 * @param string $itemFilePath
	 */
	public function setItemFilePath($itemFilePath)
	{
		$this->dayIntervalTemplatePath = $itemFilePath;
	}



	/**
	 * @return \DateTime
	 */
	public function getStartDate()
	{
		return $this->startDate;
	}



	/**
	 * @return \DateTime
	 */
	public function getEndDate()
	{
		return $this->endDate;
	}



	/**
	 * @return \Atreo\Calendar\Day[]
	 */
	public function getDays()
	{
		return $this->days;
	}



	/**
	 * @return \Atreo\Calendar\DayInterval[]
	 */
	public function getDayIntervals()
	{
		return $this->dayIntervals;
	}

}



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class CalendarException extends \Exception {}



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class InvalidDayIntervalDatesException extends CalendarException {};